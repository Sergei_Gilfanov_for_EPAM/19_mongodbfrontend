package com.epam.javatraining2016.springmvcfrontend.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.Client;
import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.Order;
import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.OrdersPaged;
import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.UserSearchResult;
import com.epam.javatraining2016.springmvcfrontend.support.CurrentUser;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@Controller
@RequestMapping(value = "/clients/{clientId}/orders")
public class Orders implements SoapUser {
  // private static final Logger log = LoggerFactory.getLogger(Clients.class);

  @GetMapping
  public ModelAndView get(@AuthenticationPrincipal CurrentUser currentUser,
      @PathVariable String clientId, @RequestParam(required = false) String from,
      @RequestParam(required = false) String status) {
    final int pageLength = 5;
    ModelAndView mav = new ModelAndView();
    mav.addObject("currentUser", currentUser);
    mav.addObject("clientId", clientId);

    OrdersPaged ordersPaged;
    if (status == null) {
      status = "";
    }

    if (from == null) {
      ordersPaged = soapEndPoint().getOrdersPaged(clientId, "", pageLength, status);
    } else {
      ordersPaged = soapEndPoint().getOrdersPaged(clientId, from, pageLength, status);
    }
    mav.addObject("ordersPaged", ordersPaged);

    if (status != null) {
      mav.addObject("statusId", String.format("status=%s&", status));
    }

    mav.setViewName("orders");
    return mav;
  }
}
