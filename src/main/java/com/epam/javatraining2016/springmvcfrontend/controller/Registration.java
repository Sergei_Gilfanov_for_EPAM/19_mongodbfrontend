package com.epam.javatraining2016.springmvcfrontend.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.UserSearchResult;
import com.epam.javatraining2016.springmvcfrontend.support.CurrentUser;
import com.epam.javatraining2016.springmvcfrontend.support.PasswordsMismatchException;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@Controller
@RequestMapping(value = "/registration")
public class Registration implements SoapUser {

  @GetMapping
  public ModelAndView get() {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("registration");
    return mav;
  }

  @PostMapping
  public ModelAndView post(@RequestParam String login, @RequestParam String fullName,
      @RequestParam String password, @RequestParam String password2) {

    if (!password.equals(password2)) {
      throw new PasswordsMismatchException();
    }

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(15);
    String encryptedPassword = encoder.encode(password2);
    soapEndPoint().createUser(login, fullName, encryptedPassword);
    ModelAndView mav = new ModelAndView();
    mav.setViewName("redirect:/clients");
    return mav;
  }

}
