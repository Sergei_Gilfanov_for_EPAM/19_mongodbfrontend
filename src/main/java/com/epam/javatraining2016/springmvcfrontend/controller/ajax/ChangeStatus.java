package com.epam.javatraining2016.springmvcfrontend.controller.ajax;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.springmvcfrontend.protocol.Id;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@RestController
@RequestMapping(value = "/backend/changeStatus")
public class ChangeStatus implements SoapUser {

  @RequestMapping(method = RequestMethod.POST)
  public Id post(@RequestParam String orderId, @RequestParam String status,
      @RequestParam String commentText) {
    String historyRecordId = soapEndPoint().changeOrderStatus(orderId, status, commentText);
    Id retval = new Id(historyRecordId);
    return retval;
  }
}
