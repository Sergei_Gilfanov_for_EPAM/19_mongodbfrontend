package com.epam.javatraining2016.springmvcfrontend.controller.ajax;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.springmvcfrontend.protocol.Client;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@RestController
@RequestMapping(value = "/backend/createClient")
public class CreateClient implements SoapUser {

  @RequestMapping(method = RequestMethod.POST)
  public Client post(@RequestParam String clientName) {
    String clientId = soapEndPoint().createClient(clientName);
    Client retval = new Client(clientId, clientName);
    return retval;
  }
}
