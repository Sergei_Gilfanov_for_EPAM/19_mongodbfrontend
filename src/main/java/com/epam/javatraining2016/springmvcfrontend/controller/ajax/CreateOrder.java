package com.epam.javatraining2016.springmvcfrontend.controller.ajax;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.springmvcfrontend.protocol.Id;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@RestController
@RequestMapping(value = "/backend/createOrder")
public class CreateOrder implements SoapUser {

  @RequestMapping(method = RequestMethod.POST)
  public Id post(@RequestParam String clientId, @RequestParam String commentText) {
    String orderId = soapEndPoint().createOrder(clientId, commentText);
    Id retval = new Id(orderId);
    return retval;
  }
}
