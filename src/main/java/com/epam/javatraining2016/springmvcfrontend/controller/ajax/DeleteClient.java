package com.epam.javatraining2016.springmvcfrontend.controller.ajax;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.springmvcfrontend.protocol.Order;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@RestController
@RequestMapping(value = "/backend/deleteClient")
public class DeleteClient implements SoapUser {

  @PostMapping
  public void post(@RequestParam String clientId) {
    soapEndPoint().deleteClient(clientId);
  }
}
