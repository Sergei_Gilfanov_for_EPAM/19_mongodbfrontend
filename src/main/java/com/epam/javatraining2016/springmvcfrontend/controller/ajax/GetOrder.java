package com.epam.javatraining2016.springmvcfrontend.controller.ajax;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.springmvcfrontend.protocol.Order;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@RestController
@RequestMapping(value = "/backend/getOrder")
public class GetOrder implements SoapUser {

  @RequestMapping(method = RequestMethod.GET)
  public Order get(@RequestParam String orderId) {
    com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.Order soapOrder =
        soapEndPoint().getOrder(orderId);
    Order retval = new Order(soapOrder.getId(), soapOrder.getStatus());
    return retval;
  }
}
