package com.epam.javatraining2016.springmvcfrontend.controller.ajax;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.springmvcfrontend.protocol.Comment;
import com.epam.javatraining2016.springmvcfrontend.protocol.StatusChange;
import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.OrderHistoryRecord;
import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.OrderHistoryRecordComment;
import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.OrderHistoryRecordStatusChange;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@RestController
@RequestMapping(value = "/backend/getOrderHistory")
public class GetOrderHistory implements SoapUser {

  @RequestMapping(method = RequestMethod.GET)
  public List<Comment> get(@RequestParam String orderId) {
    List<OrderHistoryRecord> orderHistory = soapEndPoint().getHistory(orderId);

    List<Comment> retval = new ArrayList<Comment>(orderHistory.size());
    for (OrderHistoryRecord historyRecord : orderHistory) {
      // Вся иерархия OrderHistoryRecord* - автогенерируемая из WSDL сервиса, поэтому
      // воспользоваться полиморфизмом пока не получилось. Будем проверять типы через
      // instanceof
      Comment toAdd = null;
      if (historyRecord instanceof OrderHistoryRecordStatusChange) {
        OrderHistoryRecordStatusChange orderHistoryRecordStatusChange =
            (OrderHistoryRecordStatusChange) historyRecord;
        StatusChange statusChange =
            new StatusChange(orderHistoryRecordStatusChange.getComment().getCommentText(),
                orderHistoryRecordStatusChange.getStatus());
        toAdd = statusChange;
      } else if (historyRecord instanceof OrderHistoryRecordComment) {
        OrderHistoryRecordComment orderHistoryRecordComment =
            (OrderHistoryRecordComment) historyRecord;
        toAdd = new Comment(orderHistoryRecordComment.getComment().getCommentText());
      }
      retval.add(toAdd);
    }
    return retval;
  }
}
