package com.epam.javatraining2016.springmvcfrontend.controller.ajax;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.springmvcfrontend.protocol.Order;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@RestController
@RequestMapping(value = "/backend/getOrders")
public class GetOrders implements SoapUser {

  @RequestMapping(method = RequestMethod.GET)
  public List<Order> get(@RequestParam String clientId) {
    List<com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.Order> soapOrders =
        soapEndPoint().getOrders(clientId);

    List<Order> retval = new ArrayList<Order>(soapOrders.size());

    for (com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.Order soapOrder : soapOrders) {
      Order order = new Order(soapOrder.getId(), soapOrder.getStatus());
      retval.add(order);
    }
    return retval;
  }
}
