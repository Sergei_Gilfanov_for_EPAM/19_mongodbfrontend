package com.epam.javatraining2016.springmvcfrontend.protocol;

public class Client {
  public String id;
  public String clientName;
  
  public Client(String id, String clientName) {
    super();
    this.id = id;
    this.clientName = clientName;
  }
}
