package com.epam.javatraining2016.springmvcfrontend.protocol;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME, 
    include = JsonTypeInfo.As.PROPERTY, 
    property = "_type")
  @JsonSubTypes({ 
    @Type(value = Comment.class, name = "comment"), 
    @Type(value = StatusChange.class, name = "statusChange") 
  })
public class Comment {
  public String comment;
  
  public Comment(String comment) {
    super();
    this.comment = comment;
  }
  
}
