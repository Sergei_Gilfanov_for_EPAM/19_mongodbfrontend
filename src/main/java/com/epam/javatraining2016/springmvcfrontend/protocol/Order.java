package com.epam.javatraining2016.springmvcfrontend.protocol;

public class Order {
  public String id;
  public String status;

  public Order(String id, String status) {
    super();
    this.id = id;
    this.status = status;
  }
}
