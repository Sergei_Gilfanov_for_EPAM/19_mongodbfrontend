package com.epam.javatraining2016.springmvcfrontend.protocol;

public class StatusChange extends Comment {
  public String status;

  public StatusChange(String comment, String status) {
    super(comment);
    this.status = status;
  }
  
}
