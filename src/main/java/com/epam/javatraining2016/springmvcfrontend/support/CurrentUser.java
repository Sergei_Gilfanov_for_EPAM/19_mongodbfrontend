package com.epam.javatraining2016.springmvcfrontend.support;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class CurrentUser extends User {
  private static final long serialVersionUID = -4776989395813888934L;

  private final String fullName;

  public CurrentUser(String username, String password,
      Collection<? extends GrantedAuthority> authorities, String fullName) {
    super(username, password, authorities);
    this.fullName = fullName;
  }

  public CurrentUser(CurrentUser proto) {
    super(proto.getUsername(), proto.getPassword(), proto.getAuthorities());
    this.fullName = proto.fullName;
  }

  public String getFullName() {
    return fullName;
  }
}
