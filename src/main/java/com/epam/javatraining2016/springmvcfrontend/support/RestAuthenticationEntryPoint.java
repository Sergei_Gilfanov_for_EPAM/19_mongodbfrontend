package com.epam.javatraining2016.springmvcfrontend.support;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
  private static final Logger log = LoggerFactory.getLogger(RestAuthenticationEntryPoint.class);

  public RestAuthenticationEntryPoint() {
    log.debug("constructor");
  }

  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response,
      AuthenticationException authException) throws IOException, ServletException {
    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    response.setContentType("application/json; charset=utf8");
    PrintWriter writer = response.getWriter();
    writer.write("{\"reason\": \"User is not logged in\"}");
  }

}
