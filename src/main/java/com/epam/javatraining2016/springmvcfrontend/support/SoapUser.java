package com.epam.javatraining2016.springmvcfrontend.support;

import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.AutoRepairShopService;
import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.AutoRepairShopServiceEndpoint;

public interface SoapUser {
  default AutoRepairShopServiceEndpoint soapEndPoint() {
    AutoRepairShopService autoRepairShopService = new AutoRepairShopService();
    AutoRepairShopServiceEndpoint endpoint = autoRepairShopService.getAutoRepairShopServiceEndpointPort();
    return endpoint;
  }
}
