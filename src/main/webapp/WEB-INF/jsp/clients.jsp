<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />

<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title></title>
	<meta name="description" content="">
	<meta name="author" content="">

	<meta name="_csrf" content="${_csrf.token}"/>
	<!-- default header name is X-CSRF-TOKEN -->
	<meta name="_csrf_header" content="${_csrf.headerName}"/>

	<link rel="stylesheet" href="${baseUrl}/css/palette.css">
	<link rel="stylesheet" href="${baseUrl}/css/styles-0.1.css">
</head>
<body>
	<div class="page-header">
		<span class="page-header_left"></span>
		<span class="page-header_right">
			<span>${currentUser.fullName}
				<sec:authorize access="hasRole('CHIEF_MANAGER')">(Администратор)</sec:authorize>
			</span>
		<a href="${baseUrl}/logout" class="page-header_logout-button">Выход</a></span>
	</div>
	<div class="client-list-page app-page">
		<h1>Список клиентов</h1>
		<p>
			<ul class="client-list">
			<c:forEach var="client" items="${clients}">
				<li class="client-list-item">
					<a class="client-name" href="${baseUrl}/clients/${client.id}/orders"><c:out value="${client.clientName}"/></a>
					<sec:authorize access="hasRole('CHIEF_MANAGER')">
						<span class="client-delete clickable-text" data-client-id="${client.id}">Удалить клиента</span>
					</sec:authorize>
				</li>
			</c:forEach>
			</ul>
			<div class="button-container"><div class="add-client-button button">Добавить клиента</div></div>
		</p>
	</div>

	<div class="popup-overlay hidden">
		<div class="client-add app-popup hidden">
			<span class="close-button"></span>
			<form method="post">
				<label>Имя нового клиента: <input type="text" name="clientName"/></label>
				<div class="button-container"><div class="button">Сохранить</div></div>
				<input type="hidden" name="<c:out value="${_csrf.parameterName}"/>" value="<c:out value="${_csrf.token}"/>"/>
			</form>
		</div>
	</div>
	<script data-main="${baseUrl}/js/app" src="${baseUrl}/js/vendor/require.js"></script>

</body>
</html>
