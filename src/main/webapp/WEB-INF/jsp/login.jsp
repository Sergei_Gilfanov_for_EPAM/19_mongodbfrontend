<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />

<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title></title>
	<meta name="description" content="">
	<meta name="author" content="">

	<link rel="stylesheet" href="${baseUrl}/css/palette.css">
	<link rel="stylesheet" href="${baseUrl}/css/styles-0.1.css">
</head>
<body>
	<div class="popup-overlay">
		<div class="user-login app-popup">
			<form action="" method="post">
				<c:if test="${not empty param.login_error}">
					<p>Пользователь не найден</p>
				</c:if>
				<c:if test="${param.logout != null}">
				<p>
					You are now logged out
				</p>
				</c:if>
				<label>Имя учетной записи: <input type="text" name="username" value='<c:if test="${not empty param.login_error}"><c:out value="${SPRING_SECURITY_LAST_USERNAME}"/></c:if>'/></label>
				<br>
				<label>Пароль: <input type="password" name="password"/></label>
				<br>
				<lable>Запомнить меня: <input type="checkbox" name="remember-me"/></lable>
				<div class="button-container"><button class="button" type="submit">Вход</button></div>
				<div><a href="registration">Регистрация</a></div>
				<input type="hidden" name="<c:out value="${_csrf.parameterName}"/>" value="<c:out value="${_csrf.token}"/>"/>
			</form>
		</div>
</body>
</html>
