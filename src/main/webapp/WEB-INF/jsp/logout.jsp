<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />

<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title></title>
	<meta name="description" content="">
	<meta name="author" content="">

	<link rel="stylesheet" href="${baseUrl}/css/palette.css">
	<link rel="stylesheet" href="${baseUrl}/css/styles-0.1.css">
</head>
<body>
	<div class="popup-overlay">
		<div class="user-logout app-popup">
			<form action=""	method="post">
				<div class="button-container"><button class="button" type="submit">Выход</button></div>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			</form>
		</div>
</body>
</html>
