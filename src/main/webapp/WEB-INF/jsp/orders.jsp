<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />

<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title></title>
	<meta name="description" content="">
	<meta name="author" content="">

	<meta name="_csrf" content="${_csrf.token}"/>
	<!-- default header name is X-CSRF-TOKEN -->
	<meta name="_csrf_header" content="${_csrf.headerName}"/>

	<link rel="stylesheet" href="${baseUrl}/css/palette.css">
	<link rel="stylesheet" href="${baseUrl}/css/styles-0.1.css">
</head>
<body>
	<div class="page-header">
		<span class="page-header_left"></span>
		<span class="page-header_right"><span>${currentUser.fullName}</span>
		<a href="${baseUrl}/logout" class="page-header_logout-button">Выход</a></span>
	</div>

	<div class="client-page app-page">
		<h1><a href="../">&larr;</a>Заказы клиента</h1>
		<p>
			<div class="filter-container">
				<select class="status-select">
					<option value="">all</option>
					<option value="new">new</option>
					<option value="waiting">waiting</option>
					<option value="pending">pending</option>
					<option value="in progress">in progress</option>
					<option value="done">done</option>
					<option value="delivered">delivered</option>
				</select>
			</div>
			<ul class="order-list">
			<c:forEach var="order" items="${ordersPaged.order}">
				<li class="order-list-item" data-order-id="${order.id}">
					<span class="order-number clickable-text">${order.id}</span> <span class="order-status">${order.status}</span>
				</li>
			</c:forEach>
			</ul>
			<div class="button-container">
				<div class="paging-buttons">
					<c:if test="${not empty ordersPaged.prevPageFrom}">
						<a href="?${statusId}from=${ordersPaged.prevPageFrom}">&larr;</a>
					</c:if>
					&emsp;
					<c:if test="${not empty ordersPaged.nextPageFrom}">
						<a href="?${statusId}from=${ordersPaged.nextPageFrom}">&rarr;</a>
					</c:if>
					</div>
				<div class="add-order-button button" data-client-id="${clientId}">Новый заказ</div>
			</div>
		</p>
	</div>


	<div class="popup-overlay hidden">
		<div class="order-add app-popup hidden">
			<span class="close-button"></span>
			<form>
				<label>Комментарий к заказу: <br>
					<textarea name="comment"></textarea>
				</label>
				<div class="button-container"><div class="button">Сохранить</div></div>
			</form>
		</div>

		<div class="order-history-popup app-popup hidden">
			<div class="close-button"></div>
			<div class="popup-content">
				<h1>История заказа</h1>
					<ul class="order-history">
						<li class="li-template order-history-item order-history-item-comment">
							<p class="history-item-comment"></p>
						</li>

						<li class="li-template order-history-item order-history-item-status-change">
							<span class="secondary-text-color">Заказ изменил статус на <span class="history-item-status"></span></span>
							<p class="history-item-comment"></p>
						</li>
						<li class="li-loading">
							<span class="loading-spinner"></span>
						</li>
					</ul>
					<form>
						<fieldset>
							<p>
								<label>Статус заказа
									<select name="order-status">
										<option value="" selected>Оставить без изменения</option>
										<option value="waiting">waiting</option>
										<option value="pending">pending</option>
										<option value="in progress">in progress</option>
										<option value="redo">redo</option>
										<option value="done">done</option>
										<option value="delivered">delivered</option>
									</select>
								</label>
							</p>
							<p>
								<label>Комментарий к заказу:<br>
									<textarea name="comment"></textarea>
								</label>
							</p>
						</fieldset>
							<p>
								<div class="button-container"><div class="button">Сохранить</div></div>
							</p>
				</form>
			</div>
		</div>
	</div>
	<script data-main="${baseUrl}/js/app" src="${baseUrl}/js/vendor/require.js"></script>

</body>
</html>
