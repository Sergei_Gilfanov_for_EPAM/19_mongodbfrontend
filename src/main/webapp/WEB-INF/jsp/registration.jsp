<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />

<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title></title>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="_csrf" content="${_csrf.token}"/>
	<!-- default header name is X-CSRF-TOKEN -->
	<meta name="_csrf_header" content="${_csrf.headerName}"/>

	<link rel="stylesheet" href="${baseUrl}/css/palette.css">
	<link rel="stylesheet" href="${baseUrl}/css/styles-0.1.css">
</head>
<body>
	<div class="popup-overlay">
		<div class="user-registration app-popup">
			<form action="" method="post">
				<label><span class="label-text">Имя учетной записи:</span><input type="text" name="login"/></label>
				<br>
				<label><span class="label-text">Имя пользователя:</span><input type="text" name="fullName"/></label>
				<br>
				<label><span class="label-text">Пароль:</span><input type="password" name="password"/></label>
				<br>
				<label><span class="label-text">Пароль(повторно):</span><input type="password" name="password2"/></label>
				<br>
				<div class="form-error-area">
					<span class="hidden login-used">Имя пользователя уже занято</span>
					<span class="hidden error">При проверке логина произошла ошибка</span>
				</div>
				<div class="button-container"><div class="add-order-button button">Зарегистрироваться</div></div>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			</form>
		</div>
		<script data-main="${baseUrl}/js/app" src="${baseUrl}/js/vendor/require.js"></script>
</body>
</html>
