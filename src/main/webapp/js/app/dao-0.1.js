define(["jquery"], function ($) {
	$csrfToken = $("meta[name='_csrf']");
	$csrfHeader = $("meta[name='_csrf_header']");

	function csrfHeaders() {
		var token = $csrfToken.attr("content");
		var header = $csrfHeader.attr("content");
		var retval = Object.create(null);
		retval[header] = token;
		return retval;
	}

	function ClientList(backendBase) {
		this.backendBase = backendBase;
		this.clientListCache = null;
	}
	ClientList.prototype.get =  function (refresh) {
		var self = this;
		var getDone = $.Deferred();

		if ( !refresh && list != null ) {
			getDone.resolve(self.clientListCache);
			return getDone;
		}

		$.get( self.backendBase + "/clientList", {} )
		.done(function(data) {
			self.clientListCache = data;
			getDone.resolve(data);
		})
		.fail(function() {
			getDone.reject();
		});
		return getDone;
	};

	// --------------
	function Client(backendBase) {
		this.backendBase = backendBase;
	}

	Client.prototype.create = function (serializedForm) {
		return $.ajax({
		  type: "POST",
		  url: this.backendBase + "/createClient",
		  data: serializedForm,
		  headers: csrfHeaders()
		});
	}

	Client.prototype.delete = function (client) {
		return $.ajax({
		  type: "POST",
		  url: this.backendBase + "/deleteClient",
		  data: {clientId: client.id},
		  headers: csrfHeaders()
		});
	}

	// -----------
	function Login(backendBase) {
		this.backendBase = backendBase;
	}

	Login.prototype.isAvailable = function(login) {
		var self = this;
		var checkDone = $.Deferred();
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		var csrf_headers = Object.create(null);
		csrf_headers[header] = token;
		$.ajax({
		  type: "POST",
		  url: self.backendBase + "/loginAvailable",
		  data: {login: login},
		  headers: csrf_headers
		})
		.done(function(data) {
			checkDone.resolve();
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			if (jqXHR.status == 409) {
				checkDone.reject('used');
			} else {
				checkDone.reject('error');
			}

		});
		return checkDone;
	}

	// --------------
	function OrderList(backendBase) {
		this.backendBase = backendBase;
		this.orderListCache = null;
	}
	OrderList.prototype.get = function (client, refresh) {
		var self = this;
		var getDone = $.Deferred();

		if (!refresh && orderListCache != null ) {
			getDone.resolve(self.orderListCache);
			return getDone;
		}

		$.get( self.backendBase + "/getOrders", {clientId: client.id} )
		.done(function(data) {
			self.orderListCache = data;
			getDone.resolve(data);
		})
		.fail(function() {
			getDone.reject();
		});
		return getDone;
	};

	// --------------
	function Order(backendBase) {
		this.backendBase = backendBase;
	}
	Order.prototype.create = function (client, messageText) {
		return $.ajax({
			  type: "POST",
			  url: this.backendBase + "/createOrder",
			  data: {clientId:client.id, commentText:messageText},
			  headers: csrfHeaders()
			});
	}

	Order.prototype.get = function (order) {
		return $.get( this.backendBase + "/getOrder", {orderId:order.id});
	}
	Order.prototype.addMessage = function(order, messageText) {
		return $.post( this.backendBase + "/createComment", {orderId:order.id, commentText:messageText});
	}

	// ------------
	function OrderHistory(backendBase) {
		this.backendBase = backendBase;
		this. orderHistoryCache = null;
	}
	OrderHistory.prototype.get = function (order, refresh) {
		var self = this;
		var getDone = $.Deferred();

		if ( !refresh && orderHistoryCache != null ) {
			getDone.resolve(self.orderHistoryCache);
			return getDone;
		}

		$.get( this.backendBase + "/getOrderHistory", {orderId: order.id} )
		.done(function(data) {
			self.orderHistoryCache = data;
			getDone.resolve(data);
		})
		.fail(function() {
			getDone.reject();
		});
		return getDone;
	};

	OrderHistory.prototype.addComment = function (order, historyItem) {
		var self = this;
		var retval = $.ajax({
			  type: "POST",
			  url: this.backendBase + "/createComment",
			  data: { orderId: order.id, commentText: historyItem.comment },
			  headers: csrfHeaders()
		})
		.done( function(data) {
			self.orderHistoryCache.push(historyItem);
		});
		return retval;
	};

	OrderHistory.prototype.changeStatus = function (order, historyItem) {
		var self = this;
		var retval = $.ajax({
			  type: "POST",
			  url: this.backendBase + "/changeStatus",
			  data: { orderId: order.id, 'status': historyItem.status, commentText: historyItem.comment },
			  headers: csrfHeaders()
		})
		.done( function(data) {
				self.orderHistoryCache.push(historyItem);
		});
		return retval;
	}

	OrderHistory.prototype.getCurrentStatus = function () {
		var self = this;
		var retval = null;
		$.each(self.orderHistoryCache, function(i, historyItem) {
			if ( historyItem._type == 'statusChange' ) {
				retval = historyItem.status;
			}
		});
		return retval;
	}

	var backendBase = "/store/backend";
	return {
			clientList: new ClientList(backendBase),
			client: new Client(backendBase),
			login: new Login(backendBase),
			orderList: new OrderList(backendBase),
			order: new Order(backendBase),
			orderHistory: new OrderHistory(backendBase)
	};
});
