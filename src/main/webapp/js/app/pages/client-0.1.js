define(["jquery", "urijs/URI", "app/dao-0.1", "app/popups-0.1"], function ($, URI, dao, popups) {
	var clientPageElement = $('.client-page');
	var orderListElement = $('.order-list');
	var $statusDroplist = $('.status-select');
	var currentURI = new URI();

	if (currentURI.hasQuery('status')) {
		$statusDroplist.val(currentURI.query(true).status);
	}

	$statusDroplist.change(function() {
		var selectedStatus = $(this).val();
		var currentURI = new URI();
		var newURI;
		if (selectedStatus === "") {
			newURI = currentURI.removeQuery("status");
		} else {
			newURI = currentURI.setQuery({status: selectedStatus});
		}
		document.location = newURI;
	});

	orderListElement.on( 'click', '.order-list-item', function() {
		var orderData = {id: $(this).data().orderId};
		var orderElement = $(this);
		popups.orderHistory.show(orderData)
		.done( function (newStatus) {
			orderElement.find('.order-status').text(newStatus);
		});
	});

	clientPageElement.find('.add-order-button').click(function() {
		popups.orderAdd.show({id:$(this).data().clientId})
		.done(function(newOrder) {
			document.location = document.location.origin + document.location.pathname;
		});
	});
});
